package webapp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/giris.do")
public class Giris  extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/WEB-INF/views/giris.jsp").forward(
				request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("action");
		
		if ("Yeni Kayit Girisi".equals(action)) 
		{
			request.getRequestDispatcher("/WEB-INF/views/Kayit.jsp").forward(
					request, response);
		} 
		else if ("Kullanici  Girisi".equals(action)) 
		{
			request.getRequestDispatcher("/WEB-INF/views/kullaniciLogin.jsp").forward(
					request, response);
		}
		
	}
}
