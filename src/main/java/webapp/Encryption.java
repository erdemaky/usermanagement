package webapp;

import javax.crypto.spec.SecretKeySpec;


import java.security.Key;
import javax.crypto.Cipher;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class Encryption {
	public String theKey = "TheBestSecretKey";
	public String algo ="AES";
	public Encryption()
	{
		
	}	
	private Key generateKey() throws Exception
	{
		 Key key = new SecretKeySpec(theKey.getBytes(),algo);
		 return key;
	}
	public String encrypt(String msg) throws Exception{
		
		Key key = generateKey();
        Cipher c = Cipher.getInstance(algo);
        c.init(Cipher.ENCRYPT_MODE,key);
        byte[] encVal = c.doFinal(msg.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);        
        return encryptedValue;
	}
	public String decrypt(String encrypted) throws Exception{
		
		  Key key = generateKey();
	        Cipher c = Cipher.getInstance(algo);
	        c.init(Cipher.DECRYPT_MODE,key);
	        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encrypted);
	        byte[] decVal = c.doFinal(decordedValue);
	        String decryptedValue = new String(decVal);
	        return decryptedValue;
	}
}