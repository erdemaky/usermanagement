package webapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(urlPatterns = "/kullaniciLogin.do")
public class loginKullanici extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
				

	}
	public String username ="";
	public String password ="";
	Encryption  enc;
	DESEncryption  desEnc;
	public  loginKullanici() {
		enc = new Encryption();
		desEnc = new DESEncryption();
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
			Connection con = null;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3307/userpassmanager","admin","hilal12958");			
			
		}catch (Exception e) {
			System.out.println("Hata 1" + e.getMessage());
		}
		
		boolean loginC= false;
		try{
			PreparedStatement st = con.prepareStatement("select * from musteri");
			ResultSet result = st.executeQuery();
			while(result.next())
			{
				String encryptedText ="";
				try {
					if(result.getString("algo").equals("AES"))
						encryptedText = enc.encrypt(request.getParameter("password"));
					else if(result.getString("algo").equals("DES"))
						encryptedText = desEnc.encrypt(request.getParameter("password"));
					else if(result.getString("algo").equals("RSA"))
						encryptedText = enc.encrypt(request.getParameter("password"));
					
				} catch (Exception e) {
					
				}
				if(result.getString("kullaniciAdi").equals(request.getParameter("username")) && result.getString("encSifre").equals(encryptedText))
				{
					
					username = request.getParameter("username");
					password = result.getString("encSifre");
					String passwordDec = request.getParameter("password");
					String id = result.getString("idmusteri");
					
					HttpSession session = request.getSession();
		             session.setAttribute("username", username);	
		             session.setAttribute("password", password);
		             session.setAttribute("id", id);
		             session.setAttribute("passwordDec", passwordDec);
		             
		             request.getRequestDispatcher("/WEB-INF/views/kullaniciBilgileri.jsp").forward(request, response);
					loginC = true;
					break;
				}
				
			}
			if(loginC == false)
			{
		           PrintWriter out= response.getWriter();
		           out.println("<font color=red>Bilgiler hatal�</font>");
		         		       
			}
		}catch (Exception e) {
			System.out.println("Hata 2" + e.getMessage());
		}		
	
		
	}
}
