package webapp;
import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@WebServlet(urlPatterns = "/KullaniciBilgileri")
public class KullaniciBilgileri extends HttpServlet {
	Encryption  enc;
	DESEncryption  desEnc;
	public  KullaniciBilgileri()
	{
		enc = new Encryption();
		desEnc = new DESEncryption();
		
	}
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("act");
		if("Guncelle".equals(action))
		{
			String username ="";
			username = request.getParameter("username");
			String encryptedText ="";
			try {
				if(request.getParameter("algo").equals("AES"))
					encryptedText = enc.encrypt(request.getParameter("password"));
				else if(request.getParameter("algo").equals("DES"))
					encryptedText = desEnc.encrypt(request.getParameter("password"));
				else if(request.getParameter("algo").equals("RSA"))
					encryptedText = enc.encrypt(request.getParameter("password"));
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			Connection con = null;
			
			try{
				Class.forName("com.mysql.jdbc.Driver");
				 con = DriverManager.getConnection("jdbc:mysql://localhost:3307/userpassmanager","admin","hilal12958");			
				
			}catch (Exception e) {
				System.out.println("Hata 1" + e.getMessage());
			}
			HttpSession session = request.getSession();
			try{
				
				String sql = "UPDATE musteri SET " +
					    "ad='" + request.getParameter("name") + "', " +
					    "soyad='" + request.getParameter("surname") + "', " +
					    "kullaniciAdi='" + request.getParameter("username") + "', " +
					    "encSifre='" + encryptedText + "', " +
					    "mailAdresi='" + request.getParameter("mail")  + "', " +
					    "algo='" + request.getParameter("algo")  + "' " +
					    " WHERE idmusteri=" + session.getAttribute("id") ;
				
				PreparedStatement update = con.prepareStatement(sql);
				update.executeUpdate();
				request.getRequestDispatcher("/WEB-INF/views/giris.jsp").forward(request, response);
			}catch (Exception e) {
				System.out.println("Hata 2" + e.getMessage());
			}
		}
		else
		{
			request.getRequestDispatcher("/WEB-INF/views/giris.jsp").forward(request, response);
		}
		
	}
}
