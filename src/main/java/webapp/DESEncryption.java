package webapp;
import java.security.Key;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class DESEncryption {
	public String theKey = "TheBestSecretKey";
	
	public DESEncryption()
	{}
	private Key generateKey() throws Exception
	{
		DESKeySpec desKeySpec = new DESKeySpec(theKey.getBytes());
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		Key key = keyFactory.generateSecret(desKeySpec);
		return key;
	}
	public String encrypt(String msg) throws Exception{
		
		Key key = generateKey();
		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		System.out.println();
		byte[] stringBytes = msg.getBytes("UTF8");
		byte[] raw = cipher.doFinal(stringBytes);
		
		BASE64Encoder encoder = new BASE64Encoder();
		String base64 = encoder.encode(raw);
		return base64;
	}
	public String decrypt(String encrypted) throws Exception{
		
		Key key = generateKey();
		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(Cipher.DECRYPT_MODE, key);
		
		BASE64Decoder decoder = new BASE64Decoder();
		
		byte[] raw = decoder.decodeBuffer(encrypted);
		byte[] stringBytes = cipher.doFinal(raw);
		
		String clear = new String(stringBytes,"UTF8");
		
		return clear;
	}
}
