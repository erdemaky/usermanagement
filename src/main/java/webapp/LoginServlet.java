package webapp;

import java.sql.*;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = "/Kayit.do")
public class LoginServlet extends HttpServlet {

	private LoginService service = new LoginService();
	Encryption  enc;
	DESEncryption  desEnc;
	public  LoginServlet()
	{
		 enc = new Encryption();
		 desEnc = new DESEncryption();
	}
	public void sqlKay�t(String ad,String soyad,String kA,String mail,String encPass,String algo)
	{
		Connection con = null;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3307/userpassmanager","admin","hilal12958");			
			
		}catch (Exception e) {
			System.out.println("Hata 1" + e.getMessage());
		}
		
		try{
			PreparedStatement ekle = con.prepareStatement("insert into musteri(ad,soyad,kullaniciAdi,mailAdresi,encSifre,algo) values(?,?,?,?,?,?)");
			ekle.setString(1, ad);
			ekle.setString(2, soyad);
			ekle.setString(3, kA);
			ekle.setString(4, mail);
			ekle.setString(5, encPass);
			ekle.setString(6, algo);
			
			ekle.executeUpdate();
		}catch (Exception e) {
			System.out.println("Hata 2" + e.getMessage());
		}
	}	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/WEB-INF/views/Kayit.jsp").forward(
				request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("act");
		if("Kaydet".equals(action))
		{
			String ad = request.getParameter("name");
			String soyad = request.getParameter("surname");
			String name = request.getParameter("username");
			String password = "123";
			String mail = request.getParameter("mail");
			String algo = request.getParameter("algo");
	
			//boolean isValidUser = service.validateUser(name, password);
	
			if (!ad.equals("") && !soyad.equals("") && !name.equals("")  && !mail.equals("") && !algo.equals("") ) {
				
				String encryptedText ="";
				try {
					if(algo.equals("AES"))
						encryptedText = enc.encrypt(password);
					else if(algo.equals("DES"))
						encryptedText = desEnc.encrypt(password);
					else if(algo.equals("RSA"))
						encryptedText = enc.encrypt(password);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				sqlKay�t(ad,soyad,name,mail,encryptedText,algo);
				request.setAttribute("name", "");	
				request.setAttribute("surname", "");	
				request.setAttribute("username", "");	
				request.setAttribute("password", "");	
				request.setAttribute("mail", "");	
				request.setAttribute("algo", "AES");	
				request.getRequestDispatcher("/WEB-INF/views/Kayit.jsp").forward(
						request, response);
			} 
			else 
			{
				request.getRequestDispatcher("/WEB-INF/views/Kayit.jsp").forward(
						request, response);
			}
		}
		else
		{
			request.getRequestDispatcher("/WEB-INF/views/giris.jsp").forward(
					request, response);
		}
	}
}